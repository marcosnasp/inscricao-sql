--
-- area_seq
-- avaliador_area_seq
-- avaliador_seq
-- avaliador_trabalho_seq
-- cidade_seq
-- conceito_seq
-- estado_seq
-- estado_seq
-- evento_seq
-- inscricao_seq
-- instituicao_ensino_seq
-- pagina_seq
-- pais_seq
-- pessoa_seq
-- registro_emissao_seq
-- resposta_avaliacao_seq
-- status_seq
-- tipo_apresentacao_seq
-- tipo_instituicao_seq
-- trabalho_seq
--

-- SEQUENCES -
-- OBS: Change the owner sequence...
create sequence area_seq minvalue 1;
alter sequence area_seq owner to desenv;

create sequence avaliador_area_seq minvalue 1;
alter sequence avaliador_area_seq owner to desenv;

create sequence avaliador_seq minvalue 1;
alter sequence avaliador_seq owner to desenv;

create sequence avaliador_trabalho_seq minvalue 1;
alter sequence avaliador_trabalho_seq owner to desenv;

create sequence cidade_seq minvalue 1;
alter sequence cidade_seq owner to desenv;

create sequence criterio_seq minvalue 1;
alter sequence criterio_seq owner to desenv;

create sequence conceito_seq minvalue 1;
alter sequence conceito_seq owner to desenv;

create sequence configuracao_seq minvalue 1;
alter sequence configuracao_seq owner to desenv;

create sequence estado_seq minvalue 1;
alter sequence estado_seq owner to desenv;

create sequence evento_seq minvalue 1;
alter sequence evento_seq owner to desenv;

create sequence inscricao_seq minvalue 1;
alter sequence inscricao_seq owner to desenv;

create sequence instituicao_ensino_seq minvalue 1;
alter sequence instituicao_ensino_seq owner to desenv;

create sequence pagina_seq minvalue 1;
alter sequence pagina_seq owner to desenv;

create sequence pais_seq minvalue 1;
alter sequence pais_seq owner to desenv;

create sequence pessoa_seq minvalue 1;
alter sequence pessoa_seq owner to desenv;

create sequence registro_emissao_seq minvalue 1;
alter sequence registro_emissao_seq owner to desenv;

create sequence resposta_avaliacao_seq minvalue 1;
alter sequence resposta_avaliacao_seq owner to desenv;

create sequence status_seq minvalue 1;
alter sequence status_seq owner to desenv;

create sequence tipo_apresentacao_seq minvalue 1;
alter sequence tipo_apresentacao_seq owner to desenv;

create sequence tipo_instituicao_seq minvalue 1;
alter sequence tipo_instituicao_seq owner to desenv;

create sequence trabalho_seq minvalue 1;
alter sequence trabalho_seq owner to desenv;
--
-- SEQUENCES -
-- OBS: Change the owner sequence...
create sequence area_seq minvalue 1;
alter sequence area_seq owner to desenv;

create sequence avaliador_area_seq minvalue 1;
alter sequence avaliador_area_seq owner to desenv;

create sequence avaliador_seq minvalue 1;
alter sequence avaliador_seq owner to desenv;

create sequence avaliador_trabalho_seq minvalue 1;
alter sequence avaliador_trabalho_seq owner to desenv;

create sequence cidade_seq minvalue 1;
alter sequence cidade_seq owner to desenv;

create sequence conceito_seq minvalue 1;
alter sequence conceito_seq owner to desenv;

create sequence estado_seq minvalue 1;
alter sequence estado_seq owner to desenv;

create sequence evento_seq minvalue 1;
alter sequence evento_seq owner to desenv;

create sequence inscricao_seq minvalue 1;
alter sequence inscricao_seq owner to desenv;

create sequence instituicao_ensino_seq minvalue 1;
alter sequence instituicao_ensino_seq owner to desenv;

create sequence pagina_seq minvalue 1;
alter sequence pagina_seq owner to desenv;

create sequence pais_seq minvalue 1;
alter sequence pais_seq owner to desenv;

create sequence pessoa_seq minvalue 1;
alter sequence pessoa_seq owner to desenv;

create sequence registro_emissao_seq minvalue 1;
alter sequence registro_emissao_seq owner to desenv;

create sequence resposta_avaliacao_seq minvalue 1;
alter sequence resposta_avaliacao_seq owner to desenv;

create sequence status_seq minvalue 1;
alter sequence status_seq owner to desenv;

create sequence tipo_apresentacao_seq minvalue 1;
alter sequence tipo_apresentacao_seq owner to desenv;

create sequence tipo_instituicao_seq minvalue 1;
alter sequence tipo_instituicao_seq owner to desenv;

create sequence trabalho_seq minvalue 1;
alter sequence trabalho_seq owner to desenv;

--
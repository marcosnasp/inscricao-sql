-- evento
-- 3
-- VII Fórum de Extensão
-- 2018-09-17 06:08:02.993000
-- 2018-09-21 09:08:12.707000
-- 3
-- 4
-- 2018-09-22 12:08:28.231000
-- 2018-09-17 12:08:49.106000
-- 2018-09-23 12:08:54.750000
-- 2018-10-30 12:09:00.246000
-- 2018-12-30 12:09:11.765000
-- true
insert into evento(id, descricao,
                   data_inicio, data_fim,
                   quantidade_trabalhos, quantidade_autor,
                   data_fim_excluir_trabalho, data_inicio_trab,
                   data_fim_trab, data_inicio_certificado,
                   data_fim_certificado, is_evento_atual)
values (nextval('evento_seq'), 'VII Fórum de Extensão', '2018-09-17 06:08:02.993000', '2018-09-21 09:08:12.707000',
        3, 4, '2018-09-22 12:08:28.231000', '2018-09-17 12:08:49.106000',
        '2018-09-23 12:08:54.750000', '2018-10-30 12:09:00.246000',
        '2018-12-30 12:09:11.765000', true);

-- id, descricao, id_evento, peso
-- para criterios

--3.1 Formatação do resumo (fonte, espaçamento, etc.)
--3.2 Identificação do projeto (título, coordenação e área temática)
--3.3 Clareza dos objetivos da ação
--3.4 Clareza do método da ação
--3.5 Clareza na apresentação dos resultados da ação

insert into criterio(id, descricao, id_evento, peso) values (nextval('criterio_seq'), 'Formatação do resumo (fonte, espaçamento, etc.)', 1, 1);
insert into criterio(id, descricao, id_evento, peso) values (nextval('criterio_seq'), 'Identificação do projeto (título, coordenação e área temática)', 1, 1);
insert into criterio(id, descricao, id_evento, peso) values (nextval('criterio_seq'), 'Clareza dos objetivos da ação', 1, 1);
insert into criterio(id, descricao, id_evento, peso) values (nextval('criterio_seq'), 'Clareza do método da ação', 1, 1);
insert into criterio(id, descricao, id_evento, peso) values (nextval('criterio_seq'), 'Clareza na apresentação dos resultados da ação', 1, 1);

-- para administradores
-- id, id_pessoa, ativo, id_evento

-- DORLENE MARIA CARDOSO DE AQUINO, 20958838372, dmcaquino@gmail.com
-- ALDEN MAKEL PONTES ALMEIDA, 03146831350, aldenmakel@hotmail.com
-- MARILENE SABINO BEZZERRA, 28007379304, marilene_sabino@yahoo.com.br
-- DAVI ALYSSON DA CRUZ ANDRADE, 04244270442, daviandrade.ufma@gmail.com
-- CLARICE PINHEIRO NEVES, 27851400368, clara.neves@hotmail.com
-- MARIA LÚCIA LEAL DE CASTRO, 04490088353, lucialealcastro@yahoo.com.br
-- TANIA LIMA DOS SANTOS, 25248642353, taniamaranhao1@gmail.com

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'DORLENE MARIA CARDOSO DE AQUINO',
          20958838372, 'dmcaquino@gmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'ALDEN MAKEL PONTES ALMEIDA',
          '03146831350', 'aldenmakel@hotmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'MARILENE SABINO BEZZERRA',
          '28007379304', 'marilene_sabino@yahoo.com.br', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'DAVI ALYSSON DA CRUZ ANDRADE',
          '04244270442', 'daviandrade.ufma@gmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'CLARICE PINHEIRO NEVES',
          '27851400368', 'clara.neves@hotmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'MARIA LÚCIA LEAL DE CASTRO',
          '04490088353', 'lucialealcastro@yahoo.com.br', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'TANIA LIMA DOS SANTOS',
          '25248642353', 'taniamaranhao1@gmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

-- para conceitos

-- para tipo_apresentacao
insert into tipo_apresentacao(id, descricao, id_evento, ativo)
  values (nextval('tipo_apresentacao_seq'), 'Apresentação Oral', 1, true);
insert into tipo_apresentacao(id, descricao, id_evento, ativo)
  values (nextval('tipo_apresentacao_seq'), 'Trabalho Escrito', 1, true);

--
-- Pessoas para serem autoras --
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'ANTONIO EVALDO ALMEIDA BARROS',
          'aevaldoabarrosabarros@gmail.com', 93518951300, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'CARLOS ANDRÉ SOUSA DUBLANTE',
          'cdublante@terra.com.br', 48265543387, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'CLAUDIA ALINE',
          'claudiaalinemonteiro@gmail.com',
          38160820259, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'CLAUDIA TERESA FRIAS RIOS',
          'ctfrios@hotmail.com', 27011275304, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'ERIKA MARTINS PEREIRA',
          'erika_mpereira@yahoo.com.br', 61863106200, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'FRANCY SOUSA COSTA RABELO',
          'pfranrabelo@ufma.br', 41784189391, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'GERMÂNIA DE SOUSA ALMEIDA',
          'germania.bezerra@ufma.br', 51102099368, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'GISELE QUARIGUASI TOBIAS LIMA',
          'gisellemovetoquariguasi@hotmail.com', 45989109334, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'HERLI DE SOUSA CARVALHO',
          'herlli@hotmail.com', 29464404353, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'JAMILA LOPES PEREIRA EMÉRITO',
          'jamilaemerito@yahoo.com.br', 15645886372, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'JOÃO FORTUNATO SOARES DE QUADROS JÚNIOR',
          'joaofjr@gmail.com', 5570927603, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'JOSENILDO CAMPOS BRUSSIO',
          'josenildobrussio@gmail.com', 67383068300, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email,  cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'LENA MARIA BARROS',
          'lenafonsecamaria@ibest.com.br', 22582096334, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email,  cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'LINDA MARIA RODRIGUES',
          'lindaufma@yahoo.com.br', 28911717304, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'LUCIANA BUGARIN CARACAS',
          'l.caracas@uol.com.br', 23552417320, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'MARCOS FÁBIO BELO MATOS',
          'marcosfmatos@yahoo.com.br', 43744826368, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'MARIA DE FÁTIMA LIRES PAIVA',
          'fatimalires@gmail.com', 6206808300, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'MARIA JOSÉ ALBUQUERQUE SANTOS',
          'maze59@ufma.br', 4056680310, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'MARILENE SABINO BEZERRA',
          'marilene_sabino@yahoo.com.br', 28007379304, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'MARISE MARÇALINA DE CASTRO SILVA ROSA',
          'mmarcalina@yahoo.com.br', 12639435353, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'MAURÍCIO EDUARDO RANGEL',
          'mauricio.rangel@ufma.br', 48827665315, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'PAULO OLIVEIRA RIOS FILHO',
          'pauloriosfilho@gmail.com', 1281362514, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'PAULO ROBERTO DA SILVA RIBEIRO',
          'pauloufma@ufma.br', 99834316615, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'POLIANA PEREIRA COSTA RABELO',
          'polipcosta@gmail.com', 63555611100, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'RAFAEL FERNANDES LOPES',
          'rafaelf@lsdi.ufma.br', 91487730349, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'RAQUEL GOMES NORONHA',
          'raquelnoronha79@gmail.com', 8055494711, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'RICARDO MAZINI BORDINI',
          'mazbord@gmail.com', 31006450068, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'ROSEMARY FERREIRA DA SILVA',
          'ros-e@uol.com.br', 13717804391, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'TÂNIA CRISTINA RIBEIRO',
          'taniagassen@yahoo.com', 22592350349, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'TANIA LIMA DOS SANTOS',
          'taniamaranhao@ig.com.br', 25248642353, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'VALÉRIA MAIA LAMEIRA',
          'valerialameira@hotmail.com', 73100617720, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'VIVIANE DE OLIVEIRA BARBOSA',
          'vivioliba@yahoo.com.br', 98834207300, '32b279922817dcca5188cf6783d28861', 90);
INSERT INTO pessoa( id, nome, email, cpf, senha, id_instituicao_ensino)
  VALUES (nextval('pessoa_seq'), 'ZARTU GIGLIO CAVALCANTI',
          'zartugiglio@uol.com.br', 12555657304, '32b279922817dcca5188cf6783d28861', 90);

-- id, descricao - 10 em diante
-- Péssimo = 1
-- Ruim = 2
-- Regular = 3
-- Bom = 4
-- Ótimo = 5
insert into conceito(id, descricao) values (nextval('conceito_seq'), 'Péssimo');
insert into conceito(id, descricao) values (nextval('conceito_seq'), 'Ruim');
insert into conceito(id, descricao) values (nextval('conceito_seq'), 'Regular');
insert into conceito(id, descricao) values (nextval('conceito_seq'), 'Bom');
insert into conceito(id, descricao) values (nextval('conceito_seq'), 'Ótimo');

-- area
INSERT INTO area (id, descricao, ativo) VALUES (nextval(''), 'Comunicação', true);
INSERT INTO area (id, descricao, ativo) VALUES (2, 'Cultura', true);
INSERT INTO area (id, descricao, ativo) VALUES (3, 'Direitos Humanos e Justiça', true);
INSERT INTO area (id, descricao, ativo) VALUES (4, 'Educação', true);
INSERT INTO area (id, descricao, ativo) VALUES (5, 'Meio Ambiente', true);
INSERT INTO area (id, descricao, ativo) VALUES (6, 'Saúde', true);
INSERT INTO area (id, descricao, ativo) VALUES (7, 'Tecnologia e Produção', true);
INSERT INTO area (id, descricao, ativo) VALUES (8, 'Trabalho', true);

--
-- area_seq
-- avaliador_area_seq
-- avaliador_seq
-- avaliador_trabalho_seq
-- cidade_seq
-- conceito_seq
-- estado_seq
-- estado_seq
-- evento_seq
-- inscricao_seq
-- instituicao_ensino_seq
-- pagina_seq
-- pais_seq
-- pessoa_seq
-- registro_emissao_seq
-- resposta_avaliacao_seq
-- status_seq
-- tipo_apresentacao_seq
-- tipo_instituicao_seq
-- trabalho_seq
--

-- limpeza base--
delete from resposta_avaliacao;
delete from criterio_conceito;
delete from criterio;
delete from avaliador_trabalho;
delete from autoria;
delete from trabalho;
delete from tipo_apresentacao;
delete from registro_emissao;
delete from inscricao;
delete from avaliador_area;
delete from avaliador;
delete from admin;
delete from evento;
delete from pessoa;
delete from conceito;
-- limpeza base--





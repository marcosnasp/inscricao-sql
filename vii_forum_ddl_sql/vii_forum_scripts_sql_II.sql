--------------------------------------
create table pergunta_avaliacao
(
	id_pergunta_avaliacao serial not null
		constraint pergunta_avaliacao_pkey
			primary key,
	texto_pergunta varchar(255) not null
)
;

comment on table pergunta_avaliacao is 'tabela com as perguntas de avaliação do trabalhos'
;

comment on column pergunta_avaliacao.id_pergunta_avaliacao is 'Chave da tabela com as perguntas de avaliação dos trabalhos.'
;

comment on column pergunta_avaliacao.texto_pergunta is 'texto da pergunta a ser respondida na avaliação'
;

alter table pergunta_avaliacao owner to inscuser
;
--------------------------------------

--------------------------------------

create table resposta_pergunta_avaliacao
(
	id_resposta_pergunta_avaliacao serial not null
		constraint resposta_pergunta_avaliacao_pkey
			primary key,
	texto_resposta varchar(255),
	id_pergunta_avaliacao integer
		constraint resposta_pergunta_avaliacao_fk
			references pergunta_avaliacao
)
;

comment on column resposta_pergunta_avaliacao.id_resposta_pergunta_avaliacao is 'Chave da resposta da pergunta da avaliação
'
;

comment on column resposta_pergunta_avaliacao.texto_resposta is 'Texto da resposta que será ligada a pergunta'
;

comment on column resposta_pergunta_avaliacao.id_pergunta_avaliacao is 'Chave que associa a resposta a pergunta'
;

alter table resposta_pergunta_avaliacao owner to inscuser
;

--------------------------------------

create sequence pergunta_avaliacao_seq minvalue 1;
alter sequence pergunta_avaliacao_seq owner to inscuser;

create sequence resposta_pergunta_avaliacao_seq minvalue 1;
alter sequence resposta_pergunta_avaliaca_seq owner to inscuser;

--------------------------------------

INSERT INTO public.pergunta_avaliacao (id_pergunta_avaliacao, texto_pergunta) VALUES (nextval('pergunta_avaliacao_seq'), 'Quantidade de Palavras do Resumo');
INSERT INTO public.pergunta_avaliacao (id_pergunta_avaliacao, texto_pergunta) VALUES (nextval('pergunta_avaliacao_seq'), 'O Resumo apresenta uma ação de extensão universitária');


INSERT INTO public.resposta_pergunta_avaliacao (id_resposta_pergunta_avaliacao, texto_resposta, id_pergunta_avaliacao) VALUES (nextval('resposta_pergunta_avaliacao_seq'), 'Menos de 200', 1);
INSERT INTO public.resposta_pergunta_avaliacao (id_resposta_pergunta_avaliacao, texto_resposta, id_pergunta_avaliacao) VALUES (nextval('resposta_pergunta_avaliacao_seq'), 'De 201 a 450', 1);
INSERT INTO public.resposta_pergunta_avaliacao (id_resposta_pergunta_avaliacao, texto_resposta, id_pergunta_avaliacao) VALUES (nextval('resposta_pergunta_avaliacao_seq'), 'De 451 a 550', 1);
INSERT INTO public.resposta_pergunta_avaliacao (id_resposta_pergunta_avaliacao, texto_resposta, id_pergunta_avaliacao) VALUES (nextval('resposta_pergunta_avaliacao_seq'), 'Mais de 550', 1);
INSERT INTO public.resposta_pergunta_avaliacao (id_resposta_pergunta_avaliacao, texto_resposta, id_pergunta_avaliacao) VALUES (nextval('resposta_pergunta_avaliacao_seq'), 'Sim', 2);
INSERT INTO public.resposta_pergunta_avaliacao (id_resposta_pergunta_avaliacao, texto_resposta, id_pergunta_avaliacao) VALUES (nextval('resposta_pergunta_avaliacao_seq'), 'Não', 2);

--------------------------------------
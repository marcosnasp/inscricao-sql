------------- LIMPEZA DOS DADOS DE AVALIACAO DO TRABALHO -------------

delete from "public"."resposta_avaliacao";
delete from "public"."resposta_pergunta_avaliacao_avaliador";
delete from "public"."avaliacao_trabalho";
delete from "public"."avaliador_trabalho";

select setval('avaliacao_trabalho_seq', 1);
select setval('avaliador_trabalho_seq', 1);
select setval('resposta_avaliacao_seq', 1);
select setval('resposta_pergunta_avaliacao_avaliador_seq', 1);

------------- LIMPEZA DOS DADOS DE AVALIACAO DO TRABALHO -------------

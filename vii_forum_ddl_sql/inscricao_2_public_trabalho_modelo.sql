--1	Banner_VII_Forum_de_Extensao_modelo.pptx
--2	Comunicacao_oral_VII_Forum_de_extensao_modelo.pptx
--3	Resumo_VII_Forum_de_Extensao_modelo.docx

insert into
  public.trabalho_modelo(id_trabalho_modelo,
                         nome_modelo_trabalho,
                         arquivo_modelo_trabalho)
  values (nextval('trabalho_modelo_seq'), 'Banner_VII_Forum_de_Extensao_modelo.pptx',
          pg_read_file('../../../../../Requisitos/Reunioes/Tarefa_6844/Banner_VII_Forum_de_Extensao_modelo.pptx')::bytea);
insert into
  public.trabalho_modelo(id_trabalho_modelo,
                         nome_modelo_trabalho,
                         arquivo_modelo_trabalho)
  values (nextval('trabalho_modelo_seq'), 'Comunicacao_oral_VII_Forum_de_extensao_modelo.pptx',
          pg_read_file('../../../../../Requisitos/Reunioes/Tarefa_6844/Comunicacao_oral_VII_Forum_de_extensao_modelo.pptx')::bytea);
insert into
  public.trabalho_modelo(id_trabalho_modelo,
                         nome_modelo_trabalho,
                         arquivo_modelo_trabalho)
  values (nextval('trabalho_modelo_seq'), 'Resumo_VII_Forum_de_Extensao_modelo.docx',
          pg_read_file('.../../../../../Requisitos/Reunioes/Tarefa_6844/Resumo_VII_Forum_de_Extensao_modelo.docx')::bytea);
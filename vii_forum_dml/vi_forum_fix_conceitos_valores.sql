---------------------------------------------

UPDATE "public"."criterio_conceito" SET "valor_conceito" = 10 WHERE "id" = 1 AND "id_criterio" = 1 AND "id_conceito" = 1;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 20 WHERE "id" = 2 AND "id_criterio" = 1 AND "id_conceito" = 2;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 30 WHERE "id" = 3 AND "id_criterio" = 1 AND "id_conceito" = 3;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 40 WHERE "id" = 4 AND "id_criterio" = 1 AND "id_conceito" = 4;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 50 WHERE "id" = 5 AND "id_criterio" = 1 AND "id_conceito" = 5;

UPDATE "public"."criterio_conceito" SET "valor_conceito" = 10 WHERE "id" = 6 AND "id_criterio" = 2 AND "id_conceito" = 1;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 20 WHERE "id" = 7 AND "id_criterio" = 2 AND "id_conceito" = 2;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 30 WHERE "id" = 8 AND "id_criterio" = 2 AND "id_conceito" = 3;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 40 WHERE "id" = 9 AND "id_criterio" = 2 AND "id_conceito" = 4;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 50 WHERE "id" = 10 AND "id_criterio" = 2 AND "id_conceito" = 5;

UPDATE "public"."criterio_conceito" SET "valor_conceito" = 10 WHERE "id" = 11 AND "id_criterio" = 3 AND "id_conceito" = 1;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 20 WHERE "id" = 12 AND "id_criterio" = 3 AND "id_conceito" = 2;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 30 WHERE "id" = 13 AND "id_criterio" = 3 AND "id_conceito" = 3;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 40 WHERE "id" = 14 AND "id_criterio" = 3 AND "id_conceito" = 4;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 50 WHERE "id" = 15 AND "id_criterio" = 3 AND "id_conceito" = 5;

UPDATE "public"."criterio_conceito" SET "valor_conceito" = 10 WHERE "id" = 16 AND "id_criterio" = 4 AND "id_conceito" = 1;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 20 WHERE "id" = 17 AND "id_criterio" = 4 AND "id_conceito" = 2;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 30 WHERE "id" = 18 AND "id_criterio" = 4 AND "id_conceito" = 3;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 40 WHERE "id" = 19 AND "id_criterio" = 4 AND "id_conceito" = 4;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 50 WHERE "id" = 20 AND "id_criterio" = 4 AND "id_conceito" = 5;

UPDATE "public"."criterio_conceito" SET "valor_conceito" = 10 WHERE "id" = 21 AND "id_criterio" = 5 AND "id_conceito" = 1;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 20 WHERE "id" = 22 AND "id_criterio" = 5 AND "id_conceito" = 2;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 30 WHERE "id" = 23 AND "id_criterio" = 5 AND "id_conceito" = 3;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 40 WHERE "id" = 24 AND "id_criterio" = 5 AND "id_conceito" = 4;
UPDATE "public"."criterio_conceito" SET "valor_conceito" = 50 WHERE "id" = 25 AND "id_criterio" = 5 AND "id_conceito" = 5;


---------------------------------------------

--- FIX DATAS DO EVENTO -----
UPDATE "public"."evento" SET 
    "data_inicio" = '2018-11-12 23:59:02.993000', 
    "data_fim" = '2018-11-14 23:59:00.000000 +00:00',
    "data_fim_excluir_trabalho" = '2018-10-25 23:59:28.231000',
    "data_inicio_trab" = '2018-09-17 00:00:49.106000',
    "data_fim_trab" = '2018-10-25 23:59:00.000000',
    "data_inicio_certificado" = '2018-09-17 00:00:49.106000',
    "data_fim_certificado" = '2018-12-30 12:09:11.765000'
WHERE
    "id" = 1;
--- FIX DATAS DO EVENTO -----

------------- LIMPEZA DOS DADOS DE AVALIACAO DO TRABALHO -------------

delete from "public"."avaliacao_trabalho";
delete from "public"."avaliador_trabalho";
delete from "public"."resposta_avaliacao";
delete from "public"."resposta_pergunta_avaliacao_avaliador";

select setval('avaliacao_trabalho_seq', 0);
select setval('avaliador_trabalho_seq', 0);
select setval('resposta_avaliacao_seq', 0);
select setval('resposta_pergunta_avaliacao_avaliador_seq', 0);

------------- LIMPEZA DOS DADOS DE AVALIACAO DO TRABALHO -------------

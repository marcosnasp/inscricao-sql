----------------------------------------------------------------------------------------------------------------------------
-- INSERT AVALIADORES QUE JA SE INSCREVERAM NO SITE DO EVENTO --
-- set schema 'public';

-- 54, José Alberto Pestana Chaves, 5 - Meio ambiente
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 54, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);

-- 123, ELIZABETH LIMA COSTA, Educação, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 123, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 180, Débora Luana Ribeiro Pessoa, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 180, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 196, Cláuberson Correa Carvalho, 4 - Educação, 1 - Comunicação
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 196, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);

-- 203, Ariane Cristina Ferreira Bernardes Neves, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 203, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 217, Ana Margarida Melo Nunes, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 217, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 230, Gisele Quariguasi Tobias Lima, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 230, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 234, Claudia Maria Pinho de Abreu Pecegueiro, 4 - Educação
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 234, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);

-- 239, Andréa Dias Neves LAgo, 4 - Educação, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 239, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 268, Sueli de Souza Costa, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 268, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 322, Elaine Cristina Silva Fernandes, 4 - Educação, 2 - Cultura
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 322, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- 341, Linda Maria Rodrigues, 2 - Cultura, 1 - Comunicação
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 341, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);

-- 357, EDNALDO DOS REIS SANTOS, 4 - Educação, 2 - Cultura
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 357, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- 404, Maria Áurea Lira Feitosa, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 404, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 622 - Claudia Aline Soares Monteiro, 3 - Direitos Humanos e Justiça, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 622, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
----------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------
-- Atualizar nome incompleto da avaliadora = 'Melissa';

update pessoa set email = 'felipesctm@hotmail.com' where id = 631;
update pessoa set nome = 'Melissa Silva Moreira Rabêlo' where nome like 'melissa' and id = 595;
----------------------------------------------------------------------------------------------------------------------------

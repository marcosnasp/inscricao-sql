---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- INSERIR TRABALHOS --
--Coordenador:
--Saulo Ribeiro dos Santos (949.198.523-04)

--Formato:
--Apresentação Oral

-- Título Resumo: Experiência e Turismo: ações do extensionista no projeto Comunidade Ativa
--
-- Título do Projeto: Comunidade Ativa
--
-- Área: Educação
--
-- Autores:
-- 1. Renata do Socorro Passos e Nubia (604.931.533-73)
-- 2. Jaqueline do Nascimento Fernandes (052.705.063-60)
-- 3. Jorge Luís Silva e Silva Junior (040.256.073-60)
-- 4. Layna Souza Santos (056.340.093-59)

------------------------------------------------------------------------------------------------------------------------------------
select inscricao.id from inscricao inner join pessoa
    on inscricao.id_pessoa = pessoa.id
where pessoa.cpf = 94919852304;

insert into trabalho(id,
                     titulo,
                     data_cadastro,
                     id_inscricao,
                     id_status,
                     id_tipo_apresentacao,
                     arquivo,
                     nome_arquivo,
                     id_area,
                     titulo_projeto,
                     apresentado,
                     data_alteracao,
                     observacao)
VALUES (nextval('trabalho_seq'),
        'Experiência e Turismo: ações do extensionista no projeto Comunidade Ativa',
        now(),
        181, -- id_inscricao => cpf -> 94919852304
        1, -- status -> Enviado
        1, -- id_tipo_apresentacao -> Apresentacao oral
        pg_read_file('trabalho_saulo/EXPERIENCIA_E_TURISMO_ACOES_DO_EXTENSIONISTA_NO_PROJETO_COMUNIDADE_ATIVA.doc')::bytea,
        'EXPERIENCIA_E_TURISMO_ACOES_DO_EXTENSIONISTA_NO_PROJETO_COMUNIDADE_ATIVA.doc',-- trabalho
        4, -- id_area -> Educação
        'Comunidade Ativa',
        false,
        now(),
        'A Preencher');

-- Avaliadores da Area da Educação - id_avaliador --
select id_avaliador from avaliador_area where id_area = 4;

-- Avaliadores da Area da Educação - id_avaliador --
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 1, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 2, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 3, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 4, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 5, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 6, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 7, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 9, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 11, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 15, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 16, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 18, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 20, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 26, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 30, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 31, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 32, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 35, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 40, currval('trabalho_seq'));

-- id pessoas - autores --
select id from pessoa where cpf in (60493153373, 05270506360, 04025607360, 05634009359);
--

-- Inserindo Autor e Co-Autores --
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 183, 'A');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 823, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 832, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 840, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 851, 'CA');
---------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- INSERIR TRABALHOS --
--

-- Trabalho (título): RELAÇÕES ENTRE O SOFRIMENTO PSÍQUICO E AS REDES SOCIAIS: ALGUMAS EXPERIÊNCIAS DE “ESCUTA” NO PLANTÃO PSICOLÓGICO DA UNITI/UFMA

-- Atenção Psicológica a Adultos e Idosos: contribuições da Psicologia à Uniti/Ufma

-- Área temática: Saúde

-- Apresentação Oral

-- Autor:

-- Jena Hanay Araujo de Oliveira (cpf: 70517819368)

-- AUTORES:

-- Rodrigo Soares Pereira (cpf: 05397122300)
-- Raphaela Alves Batista (cpf: 04303123340)
-- Aline Roseany Costa Borges (cpf: 05138401335)

---------------------------------------------------------------------------------------------------------------------------------------------------------------

select inscricao.id from inscricao inner join pessoa
    on inscricao.id_pessoa = pessoa.id
where pessoa.cpf = 70517819368;

insert into trabalho(id,
                     titulo,
                     data_cadastro,
                     id_inscricao,
                     id_status,
                     id_tipo_apresentacao,
                     arquivo,
                     nome_arquivo,
                     id_area,
                     titulo_projeto,
                     apresentado,
                     data_alteracao,
                     observacao)
VALUES (nextval('trabalho_seq'),
        'RELAÇÕES ENTRE O SOFRIMENTO PSÍQUICO E AS REDES SOCIAIS: ALGUMAS EXPERIÊNCIAS DE \"ESCUTA\" NO PLANTÃO PSICOLÓGICO DA UNITI/UFMA',
        now(),
        494, -- id_inscricao => cpf -> 70517819368
        1, -- status -> Enviado
        1, -- id_tipo_apresentacao -> Apresentacao oral
        pg_read_file('trabalho_jena/Resumo_Relações_entre_Sofrimento_Psíquico_e_Redes Sociais.doc')::bytea, -- trabalho
        'Resumo_Relações_entre_Sofrimento_Psíquico_e_Redes Sociais.doc',
        6, -- id_area -> saude
        'Atenção Psicológica a Adultos e Idosos: contribuições da Psicologia à Uniti/Ufma',
        false,
        now(),
        'A Preencher');

-- Avaliadores da Area da Saúde - id_avaliador --
select id_avaliador from avaliador_area where id_area = 6;

-- Avaliadores da Area da Educação - id_avaliador --
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 1, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 2, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 3, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 4, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 5, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 6, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 7, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 9, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 10, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 12, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 13, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 14, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 16, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 17, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 21, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 28, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 33, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 34, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 36, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 39, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 43, currval('trabalho_seq'));

-- id pessoas - autores --
select id from pessoa where cpf in (70517819368, 05397122300, 04303123340, 05138401335);
--

-- Inserindo Autor e Co-Autores --
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 453, 'A');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 506, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 483, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 492, 'CA');

---------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------

-- INSERIR TRABALHOS --
--

-- Trabalho (título): O PLANTÃO PSICOLÓGICO DA UNITI/UFMA E A PREVENÇÃO AO SUICÍDIO: PANORAMA DA DEMANDA DURANTE A CAMPANHA DO SETEMBRO AMARELO

-- Atenção Psicológica a Adultos e Idosos: contribuições da Psicologia à Uniti/Ufma

-- Área temática: Saúde

-- Apresentação Oral

-- Autor:

-- Jena Hanay Araujo de Oliveira (cpf: 70517819368)

-- AUTORES:

-- Rodrigo Soares Pereira (cpf: 05397122300)
-- Aline Roseany Costa Borges (cpf: 05138401335)
-- Natale Ribeiro Meireles (cpf: 05289189319)

---------------------------------------------------------------------------------------------------------------------------------------------------------------

select inscricao.id from inscricao inner join pessoa
    on inscricao.id_pessoa = pessoa.id
where pessoa.cpf = 70517819368;

insert into trabalho(id,
                     titulo,
                     data_cadastro,
                     id_inscricao,
                     id_status,
                     id_tipo_apresentacao,
                     arquivo,
                     nome_arquivo,
                     id_area,
                     titulo_projeto,
                     apresentado,
                     data_alteracao,
                     observacao)
VALUES (nextval('trabalho_seq'),
        'RELAÇÕES ENTRE O SOFRIMENTO PSÍQUICO E AS REDES SOCIAIS: ALGUMAS EXPERIÊNCIAS DE \"ESCUTA\" NO PLANTÃO PSICOLÓGICO DA UNITI/UFMA',
        now(),
        494, -- id_inscricao => cpf -> 94919852304
        1, -- status -> Enviado
        1, -- id_tipo_apresentacao -> Apresentacao oral
        pg_read_file('trabalho_jena/Resumo_Relações_entre_Sofrimento_Psíquico_e_Redes Sociais.doc')::bytea, -- trabalho
        'Resumo_Relações_entre_Sofrimento_Psíquico_e_Redes Sociais.doc',
        6, -- id_area -> Saúde
        'Atenção Psicológica a Adultos e Idosos: contribuições da Psicologia à Uniti/Ufma',
        false,
        now(),
        'A Preencher');

-- Avaliadores da Area da Saúde - id_avaliador --
select id_avaliador from avaliador_area where id_area = 6;

--
-- id pessoas - autores --
select id from pessoa where cpf in (70517819368, 05397122300, 05289189319, 05138401335);
--

-- Inserindo Autor e Co-Autores --
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 453, 'A');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 462, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 506, 'CA');
insert into autoria(id, id_trabalho, id_pessoa, tipo) VALUES (nextval('autoria_seq'), currval('trabalho_seq'), 483, 'CA');

-- Avaliadores da Area da Educação - id_avaliador --
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 1, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 2, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 3, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 4, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 5, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 6, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 7, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 9, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 10, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 12, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 13, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 14, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 16, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 17, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 21, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 28, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 33, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 34, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 36, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 39, currval('trabalho_seq'));
insert into avaliador_trabalho(id, id_avaliador, id_trabalho) VALUES (nextval('avaliador_trabalho_seq'), 43, currval('trabalho_seq'));
--

---------------------------------------------------------------------------------------------------------------------------------------------------------------
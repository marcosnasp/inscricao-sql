
insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'DORLENE MARIA CARDOSO DE AQUINO',
          20958838372, 'dmcaquino@gmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'ALDEN MAKEL PONTES ALMEIDA',
          '03146831350', 'aldenmakel@hotmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'MARILENE SABINO BEZZERRA',
          '28007379304', 'marilene_sabino@yahoo.com.br', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'DAVI ALYSSON DA CRUZ ANDRADE',
          '04244270442', 'daviandrade.ufma@gmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'CLARICE PINHEIRO NEVES',
          '27851400368', 'clara.neves@hotmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'MARIA LÚCIA LEAL DE CASTRO',
          '04490088353', 'lucialealcastro@yahoo.com.br', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);

insert into pessoa(id, nome, cpf, email, senha, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'TANIA LIMA DOS SANTOS',
          '25248642353', 'taniamaranhao1@gmail.com', 'a424265d08b0b573f699d12067cf6beb', 90);
insert into admin(id, id_pessoa, ativo, id_evento) values (nextval('admin_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador(id, id_pessoa, ativo, id_evento) values (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) values (nextval('avaliador_area_seq'), currval('avaliador_seq'), 8);
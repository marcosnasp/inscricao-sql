INSERT INTO public.area (id, descricao, ativo) VALUES (1, 'Comunicação', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (2, 'Cultura', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (3, 'Direitos Humanos e Justiça', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (4, 'Educação', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (5, 'Meio Ambiente', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (6, 'Saúde', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (7, 'Tecnologia e Produção', true);
INSERT INTO public.area (id, descricao, ativo) VALUES (8, 'Trabalho', true);
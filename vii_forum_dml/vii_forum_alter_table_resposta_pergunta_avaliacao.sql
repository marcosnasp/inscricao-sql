---------------------------------

ALTER TABLE "public"."resposta_pergunta_avaliacao" ADD "valor_resposta_conceito" double precision NULL;
ALTER TABLE "public"."resposta_pergunta_avaliacao" ADD "eh_eliminatorio" boolean NULL;

---------------------------------

---------------------------------

UPDATE "public"."resposta_pergunta_avaliacao" SET "valor_resposta_conceito" = NULL, "eh_eliminatorio" = true WHERE "id_resposta_pergunta_avaliacao" = 1;
UPDATE "public"."resposta_pergunta_avaliacao" SET "valor_resposta_conceito" = 20, "eh_eliminatorio" = false WHERE "id_resposta_pergunta_avaliacao" = 2;
UPDATE "public"."resposta_pergunta_avaliacao" SET "valor_resposta_conceito" = 30, "eh_eliminatorio" = false WHERE "id_resposta_pergunta_avaliacao" = 3;
UPDATE "public"."resposta_pergunta_avaliacao" SET "valor_resposta_conceito" = NULL, "eh_eliminatorio" = true WHERE "id_resposta_pergunta_avaliacao" = 4;
UPDATE "public"."resposta_pergunta_avaliacao" SET "valor_resposta_conceito" = 10, "eh_eliminatorio" = false WHERE "id_resposta_pergunta_avaliacao" = 5;
UPDATE "public"."resposta_pergunta_avaliacao" SET "valor_resposta_conceito" = NULL, "eh_eliminatorio" = true WHERE "id_resposta_pergunta_avaliacao" = 6;

------------------------------------

---------------------------------------------------------------------------------------------------------------
-- CREATE TABLE avaliacao_trabalho...

create table avaliacao_trabalho
(
	id serial not null
		constraint avaliacao_trabalho_pkey
			primary key,
	id_resposta_avaliacao integer
		constraint id_resposta_avalicao
			references resposta_avaliacao,
	id_pergunta_avaliacao integer
		constraint id_pergunta_avaliacao
			references pergunta_avaliacao,
	id_resposta_pergunta_avaliacao integer
		constraint id_resposta_pergunta_avaliacao
			references resposta_pergunta_avaliacao
);

alter table avaliacao_trabalho owner to desenv;
---------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------
-- CREATE TABLE resposta_pergunta_avaliacao_avaliador...

create table resposta_pergunta_avaliacao_avaliador
(
	id serial not null
		constraint resposta_pergunta_avaliacao_avaliador_pkey
			primary key,
	id_resposta_pergunta_avaliacao integer
		constraint fk_id_resposta_pergunta_avaliacao
			references resposta_pergunta_avaliacao,
	id_avaliador_trabalho integer
		constraint fk_id_avaliador_trabalho
			references avaliador_trabalho,
	id_avaliacao_trabalho integer
		constraint fk_id_avaliacao_trabalho
			references avaliacao_trabalho
)
;

alter table resposta_pergunta_avaliacao_avaliador owner to desenv;

---------------------------------------------------------------------------------------------------------------

------------------------------------
ALTER TABLE "public"."resposta_avaliacao" ADD "id_avaliacao_trabalho" int NULL;
ALTER TABLE "public"."resposta_avaliacao" ADD CONSTRAINT "id_avaliacao_trabalho"
	FOREIGN KEY (id_avaliacao_trabalho)
	REFERENCES avaliacao_trabalho (id);
------------------------------------
---------------------------------------------------------------------------------------------------------------


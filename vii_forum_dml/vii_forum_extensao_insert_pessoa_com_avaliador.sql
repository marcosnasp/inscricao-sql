----------------------------------------------------------------------------------------------------------------------------
-- INSCREVER AVALIADORES NO EVENTO, COM SUAS AS RESPECTIVAS AREAS DE AVALIACAO --
set schema 'public';

---
-- CPF novos avaliadores para inscricao:
select id, cpf
from pessoa
where cpf in (84418664300,
              38160820259,
              71182896391,
              1487888317,
              82321361387,
              70525757368,
              70587604387,
              76642860349,
              671592336,
              70948569387,
              77402308391,
              62473980349,
              25259008391,
              97674389391,
              83242139372,
              22592350349,
              72611790353,
              77895266420);

---

-------------------------------------------------
-- 4 - Educação
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Ana Zilda dos Santos Cabral Figueredo', 84418664300, 'eba35ce238701e8cebd61b1177e4f8f0', 'anazcabral@hotmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);

-- INSCRICAO --
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 629, now(), now(), 1, null);

-------------------------------------------------

-------------------------------------------------
-- 3 - Direitos Humanos e Justiça, 6 - Saúde
-- INSCRITO
/* insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Cláudia Aline Soares Monteiro', 38160820259, 'c2eb38b583c0aaf2133ee7643826cfe3', 'claudiaalinemonteiro@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6); */
-------------------------------------------------

-------------------------------------------------
-- 5 - Meio Ambiente, 3 - Direitos Humanos e Justiça
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Evaristo José de lima Neto', 71182896391, '1fd4ef3e61cd5091f609f31324f8b25f','ejlneto@gmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 630, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 5 - Meio Ambiente, 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Felipe Sávio Cardoso Teles Monteiro', 1487888317, 'ab07521482c80bb56418e677a5345cff', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 631, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 2 - Cultura, 5 - Meio Ambiente
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Josoaldo Lima Rego', 82321361387, 'b8a3c1ce5cf765e1ab973204418a982f', 'josoaldorego@yahoo.com.br', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 632, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 2 - Cultura
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), ' Lúcio Carlos Dias oliveira', 70525757368, '9e96590b84b71409e6d92c9fa45c43c3', 'lucio.oliveira@ufma.br', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 633, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Luiza Carvalho de Oliveira', 70587604387, 'b7c54658e887f947a616c4d27cde3df9', 'luiza.ufma@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 634, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 1 - Comunicação
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Francisca da Silva', 76642860349, '02be09a281c95b7a4fe253758c1eed33', 'masilva8@yahoo.com.br', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 635, now(), now(), 1, null);

-------------------------------------------------

-------------------------------------------------
-- 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Marisa Cristina Aranha Batista', 671592336, '703ac40129142fae7ca4d0cef3d23840', ' crisaranha@hotmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 636, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 3 - Direitos Humanos e Justiça, 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Aparecida de Almeida Araujo', 70948569387, '27373e1e4c57a72653c930bbd3cbe22d', 'ostsukaraujo@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 637, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 2 - Cultura
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maira Teresa Gonçalves Rocha', 77402308391, 'edc347f0c5dc4ad668ce8ea1438524ff', 'mairatgr2@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 638, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Patricia de Maria Silva Figueiredo', 62473980349, '66641e32e468c58fe4331852d38d98ac', 'figueiredo.patricia@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 639, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 2 - Cultura, 5 - Direitos Humanos
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Plínio Santos Fontenelle', 25259008391, 'd6ed778f7a50c0138a7d8892db2d3bf9', 'fontenelleplinio@ig.com.br', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 640, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 2 - Cultura, 7 - Tecnologia e Produção
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Roosewelt Lins Silva', 97674389391, 'df6167db8ae33edaa8b27cce250e9adb', 'rooseweltl@gmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 7);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 641, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Selma do Nascimento Silva', 83242139372, '1e60f6976e15963ee433d06f9108ee4a', 'selmansilva@oi.com.br', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 642, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 2 - Cultura
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Tânia Cristina Costa Ribeiro', 22592350349, 'fefb235c38b4ca24a7f89cccddfdf784', 'tania.ribeiro@ufma.br', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 643, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 1 - Trabalho, 7 - Tecnologia e Produção
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Sérgio Sampaio Cutrim', 72611790353, '3d6f733e305b3c110a210b324e3f028d', 'sscutrim@gmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 7);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 644, now(), now(), 1, null);
-------------------------------------------------

-------------------------------------------------
-- 5 - Meio Ambiente, 7 - Tecnologia e Produção
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Wendell Ferreira de La Salles', 77895266420, 'd86bee4daa6f238d7ccb9efd924e75b9', 'wendellsalles@hotmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 7);

-- INSCRICAO -- 
insert into inscricao(id, id_pessoa, data_cadastro, 
  data_alteracao, id_evento, observacao) VALUES (nextval('inscricao_seq'), 645, now(), now(), 1, null);
----------------------------------------------------------------------------------------------------------------------------
------------------------------ INSERT AVALIADORES ---------------------------------
-- id_instituicao_ensino = 90
select * from pessoa where cpf in (22581286334, 92168388334, 86955055534, 84418664300,
    38160820259, 24977063368, 4528457342, 64662519349, 80739423304, 734072325, 17835003368, 71182896391,
    1487888317, 45989109334, 82321361387, 27527360372, 28911717304, 70525757368, 70587604387, 73875457315,
		76642860349, 62475541334, 671592336, 70948569387, 77402308391, 62473980349, 25259008391, 97674389391,
		83242139372, 5233026813, 22592350349, 24675350372, 72611790353, 77895266420);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Ana Margarida Melo Nunes', 22581286334, '4de87d9f5cca00e14b2bada192fa1027', 'amarg@globo.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Ariane Cristina Ferreira Bernardes Neves', 92168388334, '299f1ea372134f84d17a0452ed39c63c', 'ariane_bernardes@hotmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo,  id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Andréa Dias Neves Lago', 86955055534, 'ffcad47f0a6e081f470481c127b1e177', 'adnlago@gmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Ana Zilda dos Santos Cabral Figueredo', 84418664300, 'eba35ce238701e8cebd61b1177e4f8f0', 'anazcabral@hotmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Cláudia Aline Soares Monteiro', 38160820259, 'c2eb38b583c0aaf2133ee7643826cfe3', 'claudiaalinemonteiro@gmail.com', 'F', 90);
-----

-----
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Cláudia Maria Pinho de Abreu Pecegueiro', 24977063368, '5d01b43c6c887165022314f3c448c308', 'claudia.pecegueiro@ufma.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), ' Cláuberson Correa Carvalho', 04528457342, '4810eab6918162fcb52a01e5aeff5dee', 'claubersoncc@gmail.com', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), ' Débora Luana Ribeiro Pessoa', 64662519349, '0d3a6cb62a744744525c533521a71823', 'debbyeluna2@yahoo.com.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Elaine Cristina Silva Fernandes', 80739423304, '7889d0058d7c729dabdce703ad0a6d3b', 'elaine_csfernandes@hotmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Ednaldo dos Reis Santos', 734072325, 'd451dad2fa0eecac29e8398604277e20', 'edreis15@hotmail.com', 'M', 90);
----

----
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Elizabeth Lima Costa', 17835003368, 'faf934159c92d990c12604281d6d6ed5', 'bet.lima@terra.com.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Evaristo José de lima Neto', 71182896391, '1fd4ef3e61cd5091f609f31324f8b25f','ejlneto@gmail.com', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Felipe Sávio Cardoso Teles Monteiro', 1487888317, 'ab07521482c80bb56418e677a5345cff', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Gisele Quariguasi Tobias Lima', 45989109334, '4933b54368631005abfc440ccee01ca3', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Josoaldo Lima Rego', 82321361387, 'b8a3c1ce5cf765e1ab973204418a982f', 'josoaldorego@yahoo.com.br', 'M', 90);
----

----
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'José Alberto Pestana Chaves', 27527360372, '9813fe7875292bfab1ecb4c9cc44a611', 'jose.alberto@ufma.br', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Linda Maria Rodrigues', 28911717304, '7638d8e1c95d0befadf2240fb89bf7f9', 'lindaufma@yahoo.com.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), ' Lúcio Carlos Dias oliveira', 70525757368, '9e96590b84b71409e6d92c9fa45c43c3', 'lucio.oliveira@ufma.br', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Luiza Carvalho de Oliveira', 70587604387, 'b7c54658e887f947a616c4d27cde3df9', 'luiza.ufma@gmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Melissa Silva Moreira Rabêlo', 73875457315, 'e5d8e73f5662f880206a6b62b2d4c1d3', 'melissamoreira@yahoo.com', 'F', 90);
----

----
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Francisca da Silva', 76642860349, '02be09a281c95b7a4fe253758c1eed33', 'masilva8@yahoo.com.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria do Socorro Evangelista Garreto', 62475541334, '89a33ad7ba49995c6a4e465a49d17bca', 'mariagarreto@hotmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Marisa Cristina Aranha Batista', 671592336, '703ac40129142fae7ca4d0cef3d23840', ' crisaranha@hotmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Aparecida de Almeida Araujo', 70948569387, '27373e1e4c57a72653c930bbd3cbe22d', 'ostsukaraujo@gmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maira Teresa Gonçalves Rocha', 77402308391, 'edc347f0c5dc4ad668ce8ea1438524ff', 'mairatgr2@gmail.com', 'F', 90);
----

----
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Patricia de Maria Silva Figueiredo', 62473980349, '66641e32e468c58fe4331852d38d98ac', 'figueiredo.patricia@gmail.com', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Plínio Santos Fontenelle', 25259008391, 'd6ed778f7a50c0138a7d8892db2d3bf9', 'fontenelleplinio@ig.com.br', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Roosewelt Lins Silva', 97674389391, 'df6167db8ae33edaa8b27cce250e9adb', 'rooseweltl@gmail.com', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Selma do Nascimento Silva', 83242139372, '1e60f6976e15963ee433d06f9108ee4a', 'selmansilva@oi.com.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Sueli de Souza Costa', 5233026813, '627e342f0b2261ecc5b7c4087d389859', 'scsueli@gmail.com', 'F', 90);
----

----
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Tânia Cristina Costa Ribeiro', 22592350349, 'fefb235c38b4ca24a7f89cccddfdf784', 'tania.ribeiro@ufma.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Áurea Lira Feitosa', 24675350372, '521dfcc4f4a5a66655151922fa7af445', 'aurealiraj@yahoo.com.br', 'F', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Sérgio Sampaio Cutrim', 72611790353, '3d6f733e305b3c110a210b324e3f028d', 'sscutrim@gmail.com', 'M', 90);

insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Wendell Ferreira de La Salles', 77895266420, 'd86bee4daa6f238d7ccb9efd924e75b9', 'wendellsalles@hotmail.com', 'M', 90);
----

----------------------------------------------------------------------------------------------------------------------------
-- INSERT AVALIADORES QUE JA SE INSCREVERAM NO SITE DO EVENTO --

-- AVALIADORES (BANCO LOCAL):

-- 54, José Alberto Pestana Chaves
-- 123, ELIZABETH LIMA COSTA
-- 180, Débora Luana Ribeiro Pessoa
-- 196, Cláuberson Correa Carvalho
-- 203, Ariane Cristina Ferreira Bernardes Neves
-- 217, Ana Margarida Melo Nunes
-- 230, Gisele Quariguasi Tobias Lima
-- 234, Claudia Maria Pinho de Abreu Pecegueiro
-- 239, Andréa Dias Neves LAgo
-- 268, Sueli de Souza Costa
-- 322, Elaine Cristina Silva Fernandes
-- 341, Linda Maria Rodrigues
-- 357, EDNALDO DOS REIS SANTOS
-- 404, Maria Áurea Lira Feitosa

-- AVALIADORES (BANCO BDSITES):

-- 217, Ana Margarida Melo Nunes
-- 239, Andréa Dias Neves LAgo
-- 203, Ariane Cristina Ferreira Bernardes Neves
-- 196, Cláuberson Correa Carvalho
-- 234, Claudia Maria Pinho de Abreu Pecegueiro
-- 180, Débora Luana Ribeiro Pessoa
-- 357, EDNALDO DOS REIS SANTOS
-- 322, Elaine Cristina Silva Fernandes
-- 123, ELIZABETH LIMA COSTA
-- 230, Gisele Quariguasi Tobias Lima
-- 54, José Alberto Pestana Chaves
-- 341, Linda Maria Rodrigues
-- 404, Maria Áurea Lira Feitosa
-- 550, MARIA DO SOCORRO EVANGELISTA GARRETO
-- 595, melissa
-- 268, Sueli de Souza Costa

-- ÁREAS

-- 1, Comunicação
-- 2, Cultura
-- 3, Direitos Humanos e Justiça
-- 4, Educação
-- 5, Meio Ambiente
-- 6, Saúde
-- 7, Tecnologia e Produção
-- 8, Trabalho

----------------------------------------------------------------------------------------------------------------------------
-- INSERT AVALIADORES QUE JA SE INSCREVERAM NO SITE DO EVENTO --

-- 54, José Alberto Pestana Chaves, 5 - Meio ambiente
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 54, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);

-- 123, ELIZABETH LIMA COSTA, Educação, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 123, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 180, Débora Luana Ribeiro Pessoa, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 180, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 196, Cláuberson Correa Carvalho, 4 - Educação, 1 - Comunicação
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 196, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);

-- 203, Ariane Cristina Ferreira Bernardes Neves, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 203, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 217, Ana Margarida Melo Nunes, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 217, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 230, Gisele Quariguasi Tobias Lima, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 230, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 234, Claudia Maria Pinho de Abreu Pecegueiro, 4 - Educação
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 234, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);

-- 239, Andréa Dias Neves LAgo, 4 - Educação, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 239, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 268, Sueli de Souza Costa, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 268, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);

-- 322, Elaine Cristina Silva Fernandes, 4 - Educação, 2 - Cultura
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 322, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- 341, Linda Maria Rodrigues, 2 - Cultura, 1 - Comunicação
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 341, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);

-- 357, EDNALDO DOS REIS SANTOS, 4 - Educação, 2 - Cultura
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 357, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);

-- 404, Maria Áurea Lira Feitosa, 6 - Saúde
insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), 404, true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
----------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------
-- Atualizar nome incompleto da avaliadora = 'Melissa';

update pessoa set nome = 'Melissa Silva Moreira Rabêlo' where nome like 'melissa' and id = 595;
----------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------
-- INSCREVER AVALIADORES NO EVENTO, COM SUAS AS RESPECTIVAS AREAS DE AVALIACAO --

-------------------------------------------------
-- 4 - Educação
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Ana Zilda dos Santos Cabral Figueredo', 84418664300, 'eba35ce238701e8cebd61b1177e4f8f0', 'anazcabral@hotmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
-------------------------------------------------

-------------------------------------------------
-- 3 - Direitos Humanos e Justiça, 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Cláudia Aline Soares Monteiro', 38160820259, 'c2eb38b583c0aaf2133ee7643826cfe3', 'claudiaalinemonteiro@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
-------------------------------------------------

-------------------------------------------------
-- 5 - Meio Ambiente, 3 - Direitos Humanos e Justiça
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Evaristo José de lima Neto', 71182896391, '1fd4ef3e61cd5091f609f31324f8b25f','ejlneto@gmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);
-------------------------------------------------

-------------------------------------------------
-- 5 - Meio Ambiente, 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Felipe Sávio Cardoso Teles Monteiro', 1487888317, 'ab07521482c80bb56418e677a5345cff', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
-------------------------------------------------

-------------------------------------------------
-- 2 - Cultura, 5 - Meio Ambiente
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Josoaldo Lima Rego', 82321361387, 'b8a3c1ce5cf765e1ab973204418a982f', 'josoaldorego@yahoo.com.br', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 2 - Cultura
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), ' Lúcio Carlos Dias oliveira', 70525757368, '9e96590b84b71409e6d92c9fa45c43c3', 'lucio.oliveira@ufma.br', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Luiza Carvalho de Oliveira', 70587604387, 'b7c54658e887f947a616c4d27cde3df9', 'luiza.ufma@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 1 - Comunicação
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Francisca da Silva', 76642860349, '02be09a281c95b7a4fe253758c1eed33', 'masilva8@yahoo.com.br', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);
-------------------------------------------------

-------------------------------------------------
-- 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Marisa Cristina Aranha Batista', 671592336, '703ac40129142fae7ca4d0cef3d23840', ' crisaranha@hotmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
-------------------------------------------------

-------------------------------------------------
-- 3 - Direitos Humanos e Justiça, 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maria Aparecida de Almeida Araujo', 70948569387, '27373e1e4c57a72653c930bbd3cbe22d', 'ostsukaraujo@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 3);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 2 - Cultura
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Maira Teresa Gonçalves Rocha', 77402308391, 'edc347f0c5dc4ad668ce8ea1438524ff', 'mairatgr2@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
-------------------------------------------------

-------------------------------------------------
-- 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Patricia de Maria Silva Figueiredo', 62473980349, '66641e32e468c58fe4331852d38d98ac', 'figueiredo.patricia@gmail.com', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
-------------------------------------------------

-------------------------------------------------
-- 2 - Cultura, 5 - Direitos Humanos
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Plínio Santos Fontenelle', 25259008391, 'd6ed778f7a50c0138a7d8892db2d3bf9', 'fontenelleplinio@ig.com.br', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
-------------------------------------------------

-------------------------------------------------
-- 2 - Cultura, 7 - Tecnologia e Produção
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Roosewelt Lins Silva', 97674389391, 'df6167db8ae33edaa8b27cce250e9adb', 'rooseweltl@gmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 7);
-------------------------------------------------

-------------------------------------------------
-- 6 - Saúde
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Selma do Nascimento Silva', 83242139372, '1e60f6976e15963ee433d06f9108ee4a', 'selmansilva@oi.com.br', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 6);
-------------------------------------------------

-------------------------------------------------
-- 4 - Educação, 2 - Cultura
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Tânia Cristina Costa Ribeiro', 22592350349, 'fefb235c38b4ca24a7f89cccddfdf784', 'tania.ribeiro@ufma.br', 'F', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 4);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 2);
-------------------------------------------------

-------------------------------------------------
-- 1 - Trabalho, 7 - Tecnologia e Produção
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Sérgio Sampaio Cutrim', 72611790353, '3d6f733e305b3c110a210b324e3f028d', 'sscutrim@gmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 7);
-------------------------------------------------

-------------------------------------------------
-- 5 - Meio Ambiente, 7 - Tecnologia e Produção
insert into pessoa(id, nome, cpf, senha, email, sexo, id_instituicao_ensino)
  values (nextval('pessoa_seq'), 'Wendell Ferreira de La Salles', 77895266420, 'd86bee4daa6f238d7ccb9efd924e75b9', 'wendellsalles@hotmail.com', 'M', 90);

insert into avaliador(id, id_pessoa, ativo, id_evento) VALUES (nextval('avaliador_seq'), currval('pessoa_seq'), true, 1);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 5);
insert into avaliador_area(id, id_avaliador, id_area) VALUES (nextval('avaliador_area_seq'), currval('avaliador_seq'), 7);
----------------------------------------------------------------------------------------------------------------------------


----
select * from instituicao_ensino where id = 12336;
insert into instituicao_ensino(id,
                               nome,
                               sigla,
                               estrangeira,
                               cnpj,
                               id_pais,
                               id_tipo,
                               permite_diploma)
values(12336, 'PLAN INTERNATIONAL', 'PLAN', false, 0, 31, 3, false);
-- auto-generated definition
create table tipo_instituicao
(
  id_tipo_instituicao integer     not null
    constraint tipo_instituicao_pkey
    primary key,
  denominacao         varchar(50) not null
);

comment on table tipo_instituicao
is 'Mapeia os tipos de instituiÃ§Ãµes que abrigam os docentes na UFRN. Essa
 entidade foi criada pois vÃ¡rios docentes eram oriundos de Ã³rgÃ£os estaduais,
 federais,etc que nÃ£o eram instituiÃ§Ãµes de ensino.';

comment on column tipo_instituicao.id_tipo_instituicao
is 'Chave Primária';

comment on column tipo_instituicao.denominacao
is 'denominacao do tipo de instituicao';

-- auto-generated definition
create table pais
(
  id_pais           integer     not null
    constraint pais_pkey
    primary key,
  nome              varchar(80) not null,
  nacionalidade     varchar(80),
  cod_pais_pingifes varchar(3)
);

comment on table pais
is 'Entidade que armazena as informações dos paises';

comment on column pais.id_pais
is 'Chave Primária';

comment on column pais.nome
is 'nome do pais';

comment on column pais.nacionalidade
is 'Designação da Nacionalidade:
                001          BRASIL		Brasileiro
                002	AFRICA DO SUL	Sul Africano
                003	ANDORRA	???
                004	AFGANISTÃO	Afegão
                005	ARGÉLIA	                Argelino
                006	ALEMANHA            Alemão
                ...';

comment on column pais.cod_pais_pingifes
is 'Campo que informa o codigo do pais parametrizado pelo pingifes(sistema do MEC/SESU).';

create index idx_pais_nacionalidade_btree
  on pais (nacionalidade);

create index idx_pais_nome_btree
  on pais (nome);

-- auto-generated definition
create table instituicao_ensino
(
  id              integer               not null
    constraint instituicao_ensino_pkey
    primary key,
  nome            varchar(120)          not null,
  sigla           varchar(12),
  estrangeira     boolean default false not null,
  cnpj            integer,
  id_pais         integer
    constraint public_pais_fkey
    references pais,
  id_tipo         integer
    constraint instituicoes_ensino_id_tipo_fkey
    references tipo_instituicao,
  permite_diploma boolean default false not null
);

comment on table instituicao_ensino
is 'Entidade que armazena as instituições de ensino';

comment on column instituicao_ensino.id
is 'Chave Primaria';

comment on column instituicao_ensino.nome
is 'nome da instituição de ensino';

comment on column instituicao_ensino.sigla
is 'sigla da instituição';

comment on column instituicao_ensino.estrangeira
is 'diz se é uma instituição estrangeira ou nacional';

comment on column instituicao_ensino.cnpj
is 'cnpj da instituição de ensino';

comment on column instituicao_ensino.id_pais
is 'pais da instituicao, se for uma instituição estrangeira o pais NÃƒO deve ser Brasil';

comment on column instituicao_ensino.id_tipo
is 'tipo de instituição';

comment on column instituicao_ensino.permite_diploma
is 'Se permite diploma ou não';

create index "IIE1"
  on instituicao_ensino (sigla);

create index fki_tipo_inst
  on instituicao_ensino (id_tipo);

create sequence tipo_instituicao_seq;
create sequence instituicao_ensino_seq;
create sequence pais_seq;


